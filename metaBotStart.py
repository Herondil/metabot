import metaBotOnDemand
import metaBotAutoNeedIdle

if __name__ == "__main__":
  botThread = metaBotOnDemand.metaBotIdle()
  newmetaBot = metaBotAutoNeedIdle.metaBotGiver()
  metaBotAutoNeedIdle.get_meta(newmetaBot)
  botThread.start()
