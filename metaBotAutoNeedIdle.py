# -*- coding: utf-8 -*-
# Bot pour chopper les métas de Riff
#
# Par Double Z., repris par Herondil
# =============================================

import irclib
import ircbot
import random
import urllib
import threading
import time

nick = "MetaBotAuto"
description = "Bot qui indique le titre en cours"
secretRoom = "#metaRoom"
server = "irc.riff-radio.org"
port = 6667
currentMeta = "empty"
newMeta = True

def get_meta(metaBotGiver):
  global currentMeta
  global newMeta
  
  threading.Timer(2.0, get_meta,(metaBotGiver,)).start()
  response = urllib.urlopen('http://api.riff-radio.org/0/meta_last.txt')

  lastMeta = response.read()
  
  if lastMeta != currentMeta :
    if " Riff - La Playlist 24/24" not in lastMeta :
      currentMeta = lastMeta
      newMeta = True
      metaBotGiver.start()
  else :
    newMeta = False

class metaBotGiver(ircbot.SingleServerIRCBot):
  
  def __init__(self):
    nick = "MetaGiver"
    global room
    global secretRoom
    global server
    global port
    global currentMeta
    
    ircbot.SingleServerIRCBot.__init__(self, [(server, port)],nick,description) 
  
  def on_welcome(self,serv,ev):
    serv.join(secretRoom)
    serv.privmsg(secretRoom,"!meta")
    #partir ou tuer le bot  
  
    
if __name__ == "__main__":
  newmetaBot = metaBotGiver()
  get_meta(newmetaBot)
  
