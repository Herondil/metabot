# -*- coding: utf-8 -*-
# Bot pour chopper les métas de Riff
#
# Par Double Z., repris par Herondil
# =============================================

import irclib
import ircbot
import random
import urllib
import threading
import time

nick = "MetaBot"
description = "Bot qui indique le titre en cours"
room = "#riff"
secretRoom = "#metaRoom"
metaRoom = "#meta"
server = "irc.riff-radio.org"
port = 6667
meta = "empty"
flood = False
intros = [
"Vous écoutez",
"Maintenant sur Riff,",
"Rock and Much More, avec"
]

class metaBotIdle(ircbot.SingleServerIRCBot):
  
  def __init__(self):
    global nick
    global room
    global secretRoom
    global metaRoom
    global server
    global port
    global meta
    global flood
    
    ircbot.SingleServerIRCBot.__init__(self, [(server, port)],nick,description) 
  
  def on_welcome(self,serv,ev):
    serv.join(room)
    serv.join(secretRoom)

  def on_pubmsg(self,serv,ev):
    auteur = irclib.nm_to_n(ev.source())
    canal = ev.target()
    message = ev.arguments()[0].split(" ")

    if message[0] == "!meta" :
      response = urllib.urlopen('http://api.riff-radio.org/0/meta_last.txt')
      meta = response.read()
      if canal == secretRoom :
        intro = intros[random.randint(0,2)]
        if flood == True :
          serv.privmsg(room,intro + meta)
        serv.privmsg(metaRoom,intro + meta)
      else :
        intro = intros[random.randint(0,2)]
        serv.privmsg(auteur,intro + meta)
      
  def on_privmsg(self,serv,ev):
    auteur = irclib.nm_to_n(ev.source())
    canal = ev.target()
    message = ev.arguments()[0].split(" ")

    if message[0] == "!meta" :
      response = urllib.urlopen('http://api.riff-radio.org/0/meta_last.txt')
      meta = response.read()
      serv.privmsg(auteur,meta)
    elif message[0] == "!flood" :
      global flood
      if flood == True :
        flood = False
        serv.privmsg(auteur,"Messages des métas sur #riff désactivés, !flood pour réactiver")
        serv.privmsg(room,"Messages des métas désactivés, envoyez moi !flood en privé pour les réactiver !")
      elif flood == False :
        flood = True
        serv.privmsg(auteur,"Messages des métas sur #riff activés, !flood pour arrêter")
        serv.privmsg(room,"Messages des métas activés, envoyez moi !flood en privé pour arrêter !")
        intro = intros[random.randint(0,2)]
        response = urllib.urlopen('http://api.riff-radio.org/0/meta_last.txt')
        #message spécial pour le live à faire
        meta = response.read()
        serv.privmsg(room,intro + meta)
    else :
      serv.privmsg(auteur,"Désolé, je ne comprends que les commandes !meta ou !flood")
      
    
  def print_meta():
    serv.privmsg(room,meta)

    
if __name__ == "__main__":
  botThread = metaBotIdle()
  botThread.start()
  
